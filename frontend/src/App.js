import {
  Routes,
  BrowserRouter as Router,
  Route
} from "react-router-dom";
import { useEffect, useState } from "react";

import { deleteBook, getAllBooks, addBook } from "./services/BookServices";
import Header from "./components/Header";
import Books from "./components/Books";
import ToggleForm from "./components/ToggleForm";
import AddBookForm from "./components/AddBookForm";

// Styles
import "./css/App.css";

function App() {
  const [showAddForm, setShowAddForm] = useState(false);
  const [books, setBooks] = useState([]);

  useEffect(() => {
    const getAll = async () => {
      const allBooks = await getAllBooks();
      setBooks(allBooks);
    };
    getAll();
  }, []);

  const handleDelete = async (id) => {
    const status = await deleteBook(id);
    status === 200 && setBooks((books) => {
      return books.filter(book => book.id !== id);
    });
  };

  const handleAdd = async (book) => {
    const id = await addBook(book);
    const newBook = { id, ...book };
    console.log(newBook);
    setBooks([...books, newBook]);
  };
  
  return (
    <div className="main">
      <Router>
        <Header />
        <ToggleForm 
          onAdd={() => setShowAddForm(!showAddForm)}
          showAdd={showAddForm}
        />
        {showAddForm && <AddBookForm onAdd={handleAdd}/>}
        <Routes>
          <Route 
            exact path="/" 
            element={
              <Books 
                books={books}
                handleDelete={handleDelete}
              />
            }
          ></Route>
        </Routes>
      </Router>
    </div>
  );
}

export default App;