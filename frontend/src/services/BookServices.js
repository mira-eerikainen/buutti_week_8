import axios from "axios";

export const getAllBooks = async () => {
  const response = await axios.get("/books");
  return response.data;
};

export const deleteBook = async (id) => {
  const response = await axios.delete(`/books/${id}`);
  return response.status;
};

export const addBook = async (book) => {
  const response = await axios.post("/books/", book);
  return response.data.id;
};
