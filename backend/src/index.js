import express from "express";
import dotenv from "dotenv";
import "express-async-errors"; // !!
import rootRouter from "./api/rootRouter.js";
import booksRouter from "./api/booksRouter.js";
import { unknownEndpoint, errorHandler, allowCors } from "./middlewares.js";
import db from "./db/db.js";

const isDev = process.env.NODE_ENV === "dev";

const app = express();
isDev && dotenv.config() && app.use(allowCors);
app.use(express.json());
app.use(express.static("build"));

// Routes:
app.use("/", rootRouter);
app.use("/books", booksRouter);
app.use(unknownEndpoint);
app.use(errorHandler);

const APP_PORT = process.env.APP_PORT || 8080;

process.env.NODE_ENV !== "test" && 
db.createTables() && 
app.listen(APP_PORT,() => {
  console.log(`Listening to ${APP_PORT}.`);
});

export default app;