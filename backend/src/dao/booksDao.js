import db from "../db/db.js";

const addBook = async (book) => {
  const randomID = Math.floor(Math.random() * Date.now());
  const params = [randomID, ...Object.values(book)];
  console.log(`Adding a new item "${params[0]}"...`);
  const result = await db.executeQuery(
    "INSERT INTO \"books\" (\"id\", \"name\", \"author\", \"read\") VALUES ($1, $2, $3, $4) RETURNING id;", 
    params
  );
  console.log(`New item "${book.name}" by "${book.author}" was added.`);
  // console.log(result);
  return result;
};

const findAllBooks = async () => {
  console.log("Fetching all books...");
  const result = await db.executeQuery(
    "SELECT * FROM \"books\";" 
  );
  console.log(`Found ${result.rows.length} book(s).`);
  return result;
};

const findOneBook = async (id) => {
  console.log(`Searching for a book with id#: ${id}...`);
  const result = await db.executeQuery(
    "SELECT * FROM \"books\" WHERE id = $1;", [id]
  );
  console.log(`Found ${result.rows.length} book(s).`);
  console.log(result.rows.length);
  if (result.rows.length !== 1) {
    const err = new Error("Internal error");
    err.name = "dbError";
    throw err;
  }
  return result.rows;
};

const deleteBook = async (id) => {
  console.log(`Searching for a book with id#: ${id}...`);
  await db.executeQuery(
    "DELETE FROM \"books\" WHERE id = $1;", [id]
  );
  console.log(`Deleted book with id# ${id}.`);
  return;
};

const updateBook = async (book) => {
  const params = [book.id, book.name, book.author, book.read];
  console.log(`Updating item with id# ${book.id}...`);
  const result = await db.executeQuery(
    "UPDATE \"books\" SET name=$2, author=$3, read=$4 WHERE id=$1;", 
    params
  );
  console.log(`Item with id# ${book.id} updated.`);
  return result;
};

export default { 
  addBook, 
  findAllBooks,
  findOneBook,
  deleteBook,
  updateBook
};